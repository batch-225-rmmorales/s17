
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


function activityOne(){
	let myName = prompt("what is your name? ");
	let myAge = prompt("how old are you? ");
	let myCity = prompt("what city do you live in? ");

	alert("Thank you for your inputs")

	console.log("My name is " + myName);
	console.log("I am " + myAge);
	console.log("I live in " + myCity);
}

activityOne();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function activityTwo(){
		console.log("1. linkin park");
		console.log("2. jayZ");
		console.log("3. mariah Carey");
		console.log("4. eraserheads");
		console.log("5. parokya ni edgar");

	}
	activityTwo();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovies() {
		console.log("The matrix " + "88%");
		console.log("toy story " + "97%");
		console.log("lion king " + "93%");
		console.log("titanic "+ "87%");
		console.log("little mermaid " + "90%");
	}
	faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};



printUsers();
// console.log(friend1);
// console.log(friend2);






