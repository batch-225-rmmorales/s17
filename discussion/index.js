// basic functions

// section: function declaration

function printName() {
	console.log("mors");
}

printName();

function showNames() {
	const functionConst = "John";
	let functionLet = "jane";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();
	// console.log(functionConst);
	// console.log(functionLet);

function showSampleAlert() {
	alert("Hello user!");
}

showSampleAlert();

let samplePrompt = prompt("enter your name: ");

alert("hello, "+ samplePrompt);

function printWelcomeMessages () {
	let firstName = prompt("enter your first name: ");
	let lastName = prompt("enter your last name: ");
	alert('welcome ' + firstName + " " + lastName);
	
}

printWelcomeMessages();